import SwiftUI
import SwiftUINavigation
import Dependencies
import IdentifiedCollections

@MainActor
final class EditLinkModel: ObservableObject {
    @Published var destination: Destination? {
        didSet {
            self.bind()
        }
    }
    @Published var focus: EditLinkView.Field?
    @Published var link: Link {
        didSet {
            self.isValid = !self.link.urlString.isEmpty
        }
    }
    
    @Published var selectedProject: Project = .none {
        didSet {
            if self.selectedProject == .createNew {
                self.selectedProject = .none
                self.destination = .createProject(CreateProjectModel())
            } else {
                self.link.projectID = self.selectedProject == .none ? nil : self.selectedProject.id.rawValue
            }
        }
    }
    
    @Dependency(\.uuid) var uuid
    @Dependency(\.projectsRepository) var projectsRepository
    
    var isValid: Bool = true
    
    private(set) var projects: IdentifiedArrayOf<Project>
    var pickerProjects: [Project] {
        var projects = projects.elements
        projects.insert(.createNew, at: 0)
        return projects
    }
    
    var onSavedProject: (IdentifiedArrayOf<Project>) -> Void = unimplemented("EditLinkModel.onSavedProject")
    
    enum Destination {
        case alert(AlertState<AlertAction>)
        case createProject(CreateProjectModel)
        case scanQRCode(ScannerCodeModel)
    }
    
    enum AlertAction {
        case confirm
    }
    
    enum Errors: Error {
        case emptyInsertion
        case invalidURL
    }
    
    init(
        destination: Destination? = nil,
        focus: EditLinkView.Field? = .title,
        link: Link,
        allProjects: IdentifiedArrayOf<Project> = []
    ) {
        self.destination = destination
        self.focus = focus
        self.link = link
        self.projects = allProjects
        if let projectId = link.projectID {
            self.selectedProject = self.projects[id: Project.ID(projectId)] ?? .none
        }
        self.bind()
    }
    
    func deleteParameters(id: QueryParameter.ID) {
        self.link.params.remove(id: id)
        if !self.link.params.isEmpty {
            let index = self.link.params.count - 1
            self.focus = .parameter(self.link.params[index].id)
        }
    }
    
    func addQueryParameterButtonTapped() {
        self.appendEmptyParams()
    }
    
    func insertLinkTapped() {
        let readStr: String?
#if os(macOS)
        let pasteboard = NSPasteboard.general
        readStr = pasteboard.pasteboardItems?.first?.string(forType: .string)
#else
        readStr = UIPasteboard.general.string
#endif
        self.tryInsertLink(from: readStr)
    }
    
    func dismissCreateProjectButtonTapped() {
        self.destination = nil
    }
    
    @discardableResult
    func createProjectButtonTapped() -> Task<Void, Error>? {
        switch self.destination {
        case let .createProject(createProjectModel):
            self.destination = nil
            return Task {
                await self.saveNewProject(name: createProjectModel.projectName)
            }
        case .alert, .scanQRCode, .none:
            return nil
        }
    }
    
    func qrCodeScannerTapped() {
        self.destination = .scanQRCode(ScannerCodeModel())
    }
    
    func dismissScanQRCodeButtonTapped() {
        self.destination = nil
    }
    
    private func saveNewProject(name: String) async {
        let trimmedName = name.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !trimmedName.isEmpty else { return }
        let project =  await self.projectsRepository.add(
            project: Project(
                id: Project.ID(self.uuid()),
                name: trimmedName
            )
        )
        self.projects.append(project)
        self.selectedProject = project
        self.link.projectID = project.id.rawValue
        
        self.onSavedProject(self.projects)
    }
    
    private func parseInsetedStringToURL(_ insertion: String?) throws {
        guard let insertion else { throw Errors.emptyInsertion }
        guard insertion.isValidURL else { throw Errors.invalidURL }
        guard let components = URLComponents(string: insertion) else { throw Errors.invalidURL }
        self.link.scheme = components.scheme ?? ""
        self.link.host = components.host ?? ""
        self.link.path = components.path
        
        if let queryItems = components.queryItems {
            let params = queryItems.map {
                QueryParameter(
                    id: QueryParameter.ID(self.uuid()),
                    name: $0.name,
                    value: $0.value ?? "value"
                )
            }
            self.link.params.removeAll(keepingCapacity: false)
            self.link.params.append(contentsOf: params)
        }
    }
    
    private func appendEmptyParams() {
        self.link.params.append(.template(id: self.uuid()))
    }
    
    private func tryInsertLink(from str: String?) {
        do {
            try self.parseInsetedStringToURL(str)
        } catch {
            self.destination = .alert(.invalidURL)
        }
    }
    
    private func bind() {
        switch self.destination {
        case let .scanQRCode(scanModel):
            scanModel.onResultReceived = { [weak self] result in
                guard let self else { return }
                self.destination = nil
                self.tryInsertLink(from: result)
            }
            scanModel.onClose = { [weak self] in
                guard let self else { return }
                self.destination = nil
            }
        case .none, .alert, .createProject:
            break
        }
    }
}

struct EditLinkView: View {
    enum Field: Hashable {
        case parameter(QueryParameter.ID)
        case title
        case info
        case scheme
        case host
        case path
    }
    @FocusState var focus: Field?
    @ObservedObject var model: EditLinkModel
    
    var body: some View {
        List {
            Section {
                VStack(alignment: .leading) {
                    HStack {
                        Text(self.model.link.urlString)
                            .foregroundColor(.gray)
                        Spacer()
                        HStack(spacing: 20.0) {
                            Button {
                                self.model.insertLinkTapped()
                            } label: {
                                Image(systemName: "arrow.right.doc.on.clipboard")
                            }
#if os(iOS)
                            Button {
                                self.model.qrCodeScannerTapped()
                            } label: {
                                Image(systemName: "qrcode.viewfinder")
                            }
#endif
                        }
                        .buttonStyle(.borderless)
                        .foregroundColor(self.model.link.theme.mainColor)
                    }
                    Text("""
                        You need to fill all fields
                        Path should starts with '/'
                        """)
                    .font(.footnote)
                    .fontWeight(self.model.isValid ? .regular : .heavy)
                    .foregroundColor(self.model.isValid ? .gray : .red)
                }
            }
            .insetGroupedStyle(header: EmptyView())
            
            Section {
                TextField("Title", text: self.$model.link.title)
                    .focused(self.$focus, equals: .title)
                TextField("Info", text: self.$model.link.info)
                    .focused(self.$focus, equals: .info)
                ThemePicker(selection: self.$model.link.theme)
            }
            .insetGroupedStyle(header: Text("Link info"))
            
            Section {
                ProjectsPicker(projects: self.model.pickerProjects,
                               selection: self.$model.selectedProject)
            }
            .insetGroupedStyle(header: Text("Project"))
            
            Section {
                TextField("Scheme", text: self.$model.link.scheme)
                    .focused(self.$focus, equals: .scheme)
                TextField("Host", text: self.$model.link.host)
                    .focused(self.$focus, equals: .host)
                TextField("Path (use with '/')", text: self.$model.link.path)
                    .focused(self.$focus, equals: .path)
            }
            .insetGroupedStyle(header: Text("Link Components"))
                               
            Section {
                ForEach(self.$model.link.params) { $parameter in
                    HStack {
                        TextField("Name", text: $parameter.name)
                            .textFieldStyle(.roundedBorder)
                        TextField("Value", text: $parameter.value)
                            .textFieldStyle(.roundedBorder)
                        Button {
                            self.model.deleteParameters(id: parameter.id)
                        } label: {
                            Image(systemName: "trash")
                        }
                    }
                }
                
                Button("New Parameter") {
                    self.model.addQueryParameterButtonTapped()
                }
            }.insetGroupedStyle(header: Text("Parameters"))
        }
        .autocorrectionDisabled(true)
        .sheet(
            unwrapping: self.$model.destination,
            case: /EditLinkModel.Destination.createProject
        ) { $model in
            NavigationStack {
                CreateProjectView(model: model)
                .padding()
                .navigationTitle("Create new project")
                .toolbar {
                    ToolbarItem(placement: .cancellationAction) {
                        Button("Cancel") {
                            self.model.dismissCreateProjectButtonTapped()
                        }
                    }
                    ToolbarItem(placement: .confirmationAction) {
                        Button("Create") {
                            self.model.createProjectButtonTapped()
                        }
                    }
                }
            }
        }
#if os(iOS)
        .sheet(
            unwrapping: self.$model.destination,
            case: /EditLinkModel.Destination.scanQRCode
        ) { $model in
            NavigationStack {
                ScannerCodeView(model: model)
                    .navigationBarTitle("Scan QR-code", displayMode: .inline)
                    .toolbar {
                        ToolbarItem(placement: .cancellationAction) {
                            Button("Cancel") {
                                self.model.dismissScanQRCodeButtonTapped()
                            }
                        }
                    }
            }
        }
        .textInputAutocapitalization(.never)
#endif
        .alert(
            unwrapping: self.$model.destination,
            case: /EditLinkModel.Destination.alert
        ) { _ in }
#if os(macOS)
        .textFieldStyle(.roundedBorder)
        .frame(minWidth: 500.0, maxWidth: 500.0, minHeight: 600.0, maxHeight: 600.0)
#endif
        .bind(self.$model.focus, to: self.$focus)
    }
}

struct ProjectsPicker: View {
    let projects: [Project]
    
    @Binding var selection: Project
    
    var body: some View {
        Picker("Select a Project", selection: $selection) {
            ForEach(projects, id: \.self) { project in
                Label(project.name, systemImage: "filemenu.and.selection")
                    .padding(4)
                    .fixedSize(horizontal: false, vertical: true)
                    .tag(project)
            }
        }
    }
}

struct ThemePicker: View {
    @Binding var selection: Theme
    
    var body: some View {
        Picker("Theme", selection: $selection) {
            ForEach(Theme.allCases) { theme in
                ZStack {
                    RoundedRectangle(cornerRadius: 4)
                        .fill(theme.mainColor)
                    Label(theme.name, systemImage: "paintpalette")
                        .padding(4)
                }
                .foregroundColor(theme.accentColor)
                .fixedSize(horizontal: false, vertical: true)
                .tag(theme)
            }
        }
    }
}

extension AlertState where Action == EditLinkModel.AlertAction {
    static let invalidURL = AlertState(
        title: TextState("Can't read an url :( "),
        message: TextState("Are you sure this link is correct?"),
        buttons: [
            .default(TextState("Ok, I try another one link"))
        ]
    )
}

struct EditLink_Previews: PreviewProvider {
    static var previews: some View {
        WithState(initialValue: Link.mock) { $link in
            EditLinkView(model: EditLinkModel(link: .mock))
        }
    }
}
