import Dependencies
import Foundation
import XCTest

@testable import Deep_Linker

@MainActor
final class EditLinkTests: XCTestCase {
    func testDeletion() {
        let id = QueryParameter.ID(UUID())
        let model = EditLinkModel(
            link: Link(
                id: Link.ID(UUID()),
                date: .now,
                params: [
                    QueryParameter(id: QueryParameter.ID(UUID()), name: "Blob", value: "blob"),
                    QueryParameter(id: id, name: "Blob 1", value: "blob 1")
                ]
            )
        )
        
        model.deleteParameters(id: id)
        XCTAssertEqual(model.link.params.count, 1)
        XCTAssertEqual(model.link.params[0].name, "Blob")
        
        XCTAssertEqual(model.focus, .parameter(model.link.params[0].id))
    }
    
    func testCreateProject() async throws {
        let model = withDependencies { dependencies in
            dependencies.uuid = .incrementing
        } operation: {
            EditLinkModel(
                link: Link(
                    id: Link.ID(UUID()),
                    date: .now,
                    projectID: nil,
                    params: [
                        QueryParameter(id: QueryParameter.ID(UUID()), name: "Blob", value: "blob")
                    ]
                )
            )
        }
        
        model.onSavedProject = { _ in }
        
        XCTAssertNil(model.link.projectID)
        model.selectedProject = .createNew
        guard case let .some(.createProject(createProjectModel)) = model.destination else {
            XCTFail()
            return
        }
        createProjectModel.projectName = "Test Project"
        try await model.createProjectButtonTapped()?.value
        
        XCTAssertNil(model.destination)
        XCTAssertEqual(model.selectedProject.name, "Test Project")
    }
    
    func testAdd() {
        let model = withDependencies { dependencies in
            dependencies.uuid = .incrementing
        } operation: {
            EditLinkModel(
                link: Link(
                    id: Link.ID(UUID()),
                    date: .now,
                    params: [
                        QueryParameter(id: QueryParameter.ID(UUID()), name: "Blob", value: "blob")
                    ]
                )
            )
        }
        
        XCTAssertEqual(model.link.params.count, 1)
        model.addQueryParameterButtonTapped()
        
        XCTAssertEqual(model.link.params.count, 2)
    }
    
    func testQrScanOpening() {
        let model = EditLinkModel(
            link: Link(
                id: Link.ID(UUID()),
                date: .now,
                params: [
                    QueryParameter(id: QueryParameter.ID(UUID()), name: "Blob", value: "blob")
                ]
            )
        )
        
        model.qrCodeScannerTapped()
        XCTAssertNotNil(model.destination)
        
        model.dismissScanQRCodeButtonTapped()
        XCTAssertNil(model.destination)
    }
    
    func testScanURLSuccess() {
        let model = withDependencies { dependencies in
            dependencies.uuid = .incrementing
        } operation: {
            EditLinkModel(
                link: Link(
                    id: Link.ID(UUID()),
                    date: .now,
                    params: []
                )
            )
        }
        
        model.qrCodeScannerTapped()
        
        guard case let .some(.scanQRCode(scanCodeModel)) = model.destination else {
            XCTFail()
            return
        }
        
        scanCodeModel.onResultReceived("https://google.com")
        
        XCTAssertEqual(model.link.urlString, "https://google.com?")
    }
    
    func testScanURLFailure() {
        let model = withDependencies { dependencies in
            dependencies.uuid = .incrementing
        } operation: {
            EditLinkModel(
                link: Link(
                    id: Link.ID(UUID()),
                    date: .now,
                    params: []
                )
            )
        }
        
        model.qrCodeScannerTapped()
        
        guard case let .some(.scanQRCode(scanCodeModel)) = model.destination else {
            XCTFail()
            return
        }
        
        scanCodeModel.onResultReceived("sdsdsd")
        
        XCTAssertNotNil(model.destination)
    }
}
